//
//  ViewController.swift
//  FunctionsAndStatics
//
//  Created by Aleksey Knysh on 3/3/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let myClass = MyClass()
        
        // функция без параметров
        print(myClass.functionWithoutParameters())
        
        // функция с дефолтными параметрами
        print(myClass.functionWithDefaultParameters(programming: myClass.programming))
        
        // функция с несколькими входящими параметрами
        print(myClass.functionWithMultipleInputParameters(learning: myClass.learning, programming: myClass.programming))
        
        // функция не возвращающая значение
        myClass.functionThatDoesNotReturnAValue()
        
        // функция возвращающая несколько опциональных значений в картедже
        print(myClass.functionThatReturnsMultipleValues(programming: myClass.programming) ?? "nil")
        
        // функция с ярлыками параметров
        print(myClass.useShortcuts(you: "Васе", needToLearn: "всю жинь!"))
        
        // функция с вариативными параметрами
        print("Сумма всех чисел = \(myClass.sumOfNumbers(numbers: 1, 44, 5, 70, 36, 15))")
        
        // функция имеющая сквозные параметры
        myClass.changeName(name: &myClass.name)
        
        // статическая функция
        MyClass.printName()
        
        // переопределяем статическую функцию в подклассе и ставим ключевое слово static, что её нельзя было переопределить в следующем подклассе, наследуемым от этого
        MySecondClass.printName()
        
        // статическая переменная
        print(MyClass.theEnd)
    }
}
