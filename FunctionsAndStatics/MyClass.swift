//
//  MyClass.swift
//  FunctionsAndStatics
//
//  Created by Aleksey Knysh on 3/3/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import Foundation

// MARK: - MyClass
class MyClass {
    var name = "Алексею"
    let learning = "Учу"
    let programming = "программирование"
    static let theEnd = "Конец"
    
    // статическая функция
    class func printName() {
        let name = "Татьяна"
        print(name)
    }
    
    // функция без параметров
    func functionWithoutParameters() -> String {
        "\(learning) \(programming)"
    }
    
    // функция с дефолтными параметрами
    func functionWithDefaultParameters(name: String = "Тима", helpToLearn: String = "помоги выучить", programming: String) -> String {
        "\(name) \(helpToLearn) \(programming)"
    }
    
    // функция с несколькими входящими параметрами
    func functionWithMultipleInputParameters(learning: String, programming: String) -> String {
        "Пусть мозг кипит и пар с ушей валит, но я всё ровно \(learning) \(programming)"
    }
    
    // функция не возвращающая значение
    func functionThatDoesNotReturnAValue(goingToDrink: String = "Хватит уже учёбы, пойду пить кофе") {
        print(goingToDrink)
    }
    
    // функция возвращающая несколько опциональных значений в картедже
    func functionThatReturnsMultipleValues(programming: String) -> (String, String, String)? {
        let tryin = "пытаюсь"
        let understand = "понять"
        return (tryin, understand, programming)
    }
    
    // функция с ярлыками параметров
    func useShortcuts(you namе: String, needToLearn programming: String) -> String {
        "Сколько мне нужно учить программирование? - \(namе) нужно учить \(programming)"
    }
    
    // функция с вариативными параметрами
    func sumOfNumbers(numbers: Int...) -> Int {
        var totalSum = 0
        for (sum) in numbers {
            totalSum += sum
        }
        return (totalSum)
    }
    
    // функция имеющая сквозные параметры
    func changeName(name: inout String) {
        var newName = name
        newName = "Сергей"
        print("Моё новое имя теперь - \(newName)")
    }
}

// MARK: - MySecondClass
class MySecondClass: MyClass {
    // переопределяем статическую функцию в подклассе и ставим ключевое слово static, что её нельзя было переопределить в следующем подклассе, наследуемым от этого
    override static func printName() {
        let name = "Андрей"
        print("теперь эта функция выводит другое имя \(name)")
    }
}
